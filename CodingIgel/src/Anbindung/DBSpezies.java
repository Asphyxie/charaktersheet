package Anbindung;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DBSpezies {

	public static String[][] getSpezies() {

		ResultSet rs = null;

		// Der erste Index ist die # des Datensatzes. Der zweite Index ist der Teil des Dataensatzes
		

		try {
			String driver = "com.mysql.jdbc.Driver";
			String url = "jdbc:mysql://localhost/charactersheet?";
			String user = "root";
			String passwort = "";
			
			Class.forName(driver);

			Connection con;
			con = DriverManager.getConnection(url, user, passwort);
			
			Statement stmt = con.createStatement();
			
			rs = stmt.executeQuery("SELECT * FROM T_Spezies GROUP BY bezeichnung");
			
			rs.last();
			String[][] spezies = new String[rs.getRow()][3];
			rs.beforeFirst();
			
			int i = 0;
				while (rs.next()) {
					for (int j = 0; j < 3;j++) {
						spezies[i][j] = rs.getString(j+1);
					}
					i++;
				}

			con.close();
			
			return spezies;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	} // Ende von getSpezies
}
