package Anbindung;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import Logik.CharakterInterface;

public class DBNationalitaet {
	
	public static String[][] getNationalities() {

		ResultSet rs = null;

		try {
			String driver = "com.mysql.jdbc.Driver";
			String url = "jdbc:mysql://localhost/charactersheet?";
			String user = "root";
			String passwort = "";
			
			Class.forName(driver);

			Connection con;
			con = DriverManager.getConnection(url, user, passwort);
			
			Statement stmt = con.createStatement();
			
			rs = stmt.executeQuery("SELECT * FROM T_Nationalitaeten");
			
			rs.last();
			String[][] nationalities = new String[rs.getRow()][3];
			rs.beforeFirst();
			
			int i = 0;
				while (rs.next()) {
					for (int j = 0; j < 3;j++) {
						nationalities[i][j] = rs.getString(j+1);
					}
					i++;
				}

			con.close();

			return nationalities;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	} // Ende von getNationalities
	
	// Methode greift auf den Prim�rschl�ssel des Charakter zu und sucht so seine Nationalit�t heraus
	public static String[] getCharakterNationalitaet(CharakterInterface ci) {

		ResultSet rs = null;

		// Stelle 0 steht f�r die CharakterID und Stelle 1 f�r die NationalitaetID
		String[] CharNat = new String[2];

		try {
			String driver = "com.mysql.jdbc.Driver";
			String url = "jdbc:mysql://localhost/charactersheet?";
			String user = "root";
			String passwort = "";
			
			Class.forName(driver);

			Connection con;
			con = DriverManager.getConnection(url, user, passwort);
			
			Statement stmt = con.createStatement();
			
			rs = stmt.executeQuery("SELECT * FROM T_Charaktere_Nationalitaeten WHERE Pf_charakter_id=" + ci.getChar_id());
			
			CharNat[0] = rs.getString(1);
			CharNat[1] = rs.getString(2);

			con.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return CharNat;

	}
} // Ende der Klasse DatenbankNationalitaet
