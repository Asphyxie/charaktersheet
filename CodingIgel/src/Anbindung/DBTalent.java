package Anbindung;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DBTalent {
	
	public static String[][] getTalente() {

		ResultSet rs = null;

		try {
			String driver = "com.mysql.jdbc.Driver";
			String url = "jdbc:mysql://localhost/charactersheet?";
			String user = "root";
			String passwort = "";
			
			Class.forName(driver);

			Connection con;
			con = DriverManager.getConnection(url, user, passwort);
			
			Statement stmt = con.createStatement();
			
			rs = stmt.executeQuery("SELECT * FROM T_Talente");
			
			rs.last();
			String[][] talente = new String[rs.getRow()][2];
			rs.beforeFirst();
			
			int i = 0;
				while (rs.next()) {
					for (int j = 0; j < 2;j++) {
						talente[i][j] = rs.getString(j+1);
					}
					i++;
				}

			con.close();
			
			return talente;
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	} // Ende von getTalente
}
