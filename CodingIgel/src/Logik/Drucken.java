package Logik;


import java.awt.*;

import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.swing.*;
import java.awt.print.*;

/*
 * Ganz ehrlich, ich habe so 0 Plan wie man was ausdruckt. Ich habe einige Tabs offen und versuche, daraus jetzt irgendwas
 * halbwegs vern�nftiges zu produzieren. Also nicht wundern, wenn der Code f�r'n Allerwertesten ist.
 */

public class Drucken implements Printable {
	private Component com;
	
	public static void printComponent(Component c) {
		new PrintUtilities(c).print();
	}
	
	public void PrintUtilities(Component com) {
		this.com = com;
	}
	
	public void print() {
		PrinterJob printJob = PrinterJob.getPrinterJob();
		PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
		PageFormat pf = printJob.pageDialog(aset);
		printJob.setPrintable(this, pf);
		if (printJob.printDialog())
			try {
				printJob.print();
			}
			catch(PrinterException pe) {
				System.out.println("Fehler: " + pe);
			}
	}
	
	public int print(Graphics g, PageFormat pageFormat, int pageIndex) {
		if (pageIndex > 0) {
			return(NO_SUCH_PAGE);
		}
		else {
			Graphics2D g2d = (Graphics2D) g;
			/*
			 * Ich muss lernen, was diese Zeilen genau machen. Hab ich so im Internet gefunden.
			 */
			// double scale = (pageFormat.getImageableWidth()/com.getWidth());
			// g2d.scale(scale, scale);
			g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
			disableDoubleBuffering(com);
			com.paint(g2d);
			enableDoubleBuffering(com);
			return(PAGE_EXISTS);
		}
	}
	
	public static void disableDoubleBuffering(Component c) {
		RepaintManager currentManager = RepaintManager.currentManager(c);
		currentManager.setDoubleBufferingEnabled(false);
	}
	
	public static void enableDoubleBuffering(Component c) {
		RepaintManager currentManager = RepaintManager.currentManager(c);
		currentManager.setDoubleBufferingEnabled(true);
	}
	
}
