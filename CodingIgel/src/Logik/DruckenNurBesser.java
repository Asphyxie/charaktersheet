package Logik;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/*
 * Ich musste sogar auf den Code der 3-Schichten-Architektur AUfgabe zur�ckgreifen, um diese Klasse zu erstellen.
 * Hier wird eine .txt File erstellt, in der die Infos mit dem Character stehen.
 */

public class DruckenNurBesser {
	
	public static void writeCharacterDocument(Charakter ci) {

		try {
			FileWriter fw = new FileWriter("character.txt", true);
			BufferedWriter bw = new BufferedWriter(fw);

			String infos = "Vorname: " + ci.getFname() + 
					"\nNachname: " + ci.getLname() +
					"\nGeburtsdatum: " + ci.getGebdat() + 
					"\nNationalit�t: " + ci.getNationality() + 
					"\nKlasse: " + ci.getRole() + 
					"\nSpezies: " + ci.getRace() + 
					"\nTalent: " + ci.getTalent() + 
					"\nGeschlecht: " + ci.getGender() + 
					"\nAlter: " + ci.getAge() + 
					"\nGr��e: " + ci.getHeight() + 
					"\nGewicht: " + ci.getWeight() + 
					"\nB-Punkte: " + ci.getBpoints() + 
					"\nA-Punkte: " + ci.getApoints();
			
			bw.write(infos);
			bw.newLine();

			bw.close();
		}

		catch (IOException e) {
			System.err.println("Fehler");
		}

	}
}