package Logik;

public class Limitation {

	public static int minMaxLimit(int min, int max, int now) {
		if (now < min) now = min;
		if (now > max) now = max;
		return now;
	}
	
	public static boolean checkCharacterLimitation(CharakterInterface c) {
		String race = c.getRace();
		String role = c.getRole();
		String[][] l = Anbindung.DBSpezies.getSpezies();
		
		for (int i = 0; i<100;i++) {
				if ((race == l[i][1])&&(role == l[i][2])) return true;
		}
		
		return false;
	}
	
}