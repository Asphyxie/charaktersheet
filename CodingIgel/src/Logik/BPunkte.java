package Logik;

public class BPunkte {
	
	public BPunkte(String char_id, int mU, int kL, int iN, int cH, int fF, int gE, int kO, int kK) {
		super();
		this.char_id = char_id;
		MU = mU;
		KL = kL;
		IN = iN;
		CH = cH;
		FF = fF;
		GE = gE;
		KO = kO;
		KK = kK;
	}
	
	String char_id;
	int MU, KL, IN, CH, FF, GE, KO, KK;

	public int getMU() {
		return MU;
	}

	public void setMU(int mU) {
		MU = mU;
	}

	public int getKL() {
		return KL;
	}

	public void setKL(int kL) {
		KL = kL;
	}

	public int getIN() {
		return IN;
	}

	public void setIN(int iN) {
		IN = iN;
	}

	public int getCH() {
		return CH;
	}

	public void setCH(int cH) {
		CH = cH;
	}

	public int getFF() {
		return FF;
	}

	public void setFF(int fF) {
		FF = fF;
	}

	public int getGE() {
		return GE;
	}

	public void setGE(int gE) {
		GE = gE;
	}

	public int getKO() {
		return KO;
	}

	public void setKO(int kO) {
		KO = kO;
	}

	public int getKK() {
		return KK;
	}

	public void setKK(int kK) {
		KK = kK;
	}
}
