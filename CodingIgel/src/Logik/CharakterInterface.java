package Logik;

public interface CharakterInterface {

	String getChar_id();

	void setChar_id(String char_id);

	String getFname();

	void setFname(String fname);

	String getLname();

	void setLname(String lname);

	String getGebdat();

	void setGebdat(String gebdat);

	String getNationality();

	void setNationality(String nationality);

	String getRole();

	void setRole(String role);

	String getRace();

	void setRace(String race);

	String getTalent();

	void setTalent(String talent);

	String getGender();

	void setGender(String gender);

	int getAge();

	void setAge(int age);

	int getHeight();

	void setHeight(int height);

	int getWeight();

	void setWeight(int weight);

	int getBpoints();

	void setBpoints(int bpoints);

	int getApoints();

	void setApoints(int apoints);

}