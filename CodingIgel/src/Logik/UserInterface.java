package Logik;

public interface UserInterface {

	String getUser_id();

	void setUser_id(String user_id);

	String getName();

	void setName(String name);
	
	String getPasswort();
	
	void setPasswort(String passwort);

}