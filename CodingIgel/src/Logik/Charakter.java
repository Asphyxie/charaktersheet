package Logik;

public class Charakter implements CharakterInterface{
	
	// Attribute
	String char_id;
	String fname;
	String lname;
	String gebdat;
	String nationality;
	String role;
	String race;
	String talent;
	String gender;
	int age;
	int height;
	int weight;
	int bpoints;
	int apoints;
	
	public Charakter(String fname, String lname, String gebdat, String nationality, String role, String race,
			String talent, String gender, int age, int height, int weight, int bpoints, int apoints) {
		super();
		this.fname = fname;
		this.lname = lname;
		this.gebdat = gebdat;
		this.nationality = nationality;
		this.role = role;
		this.race = race;
		this.talent = talent;
		this.gender = gender;
		this.age = age;
		this.height = height;
		this.weight = weight;
		this.bpoints = bpoints;
		this.apoints = apoints;
	}
	
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#getChar_id()
	 */
	public String getChar_id() {
		return char_id;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#setChar_id(java.lang.String)
	 */
	public void setChar_id(String char_id) {
		this.char_id = char_id;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#getFname()
	 */
	public String getFname() {
		return fname;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#setFname(java.lang.String)
	 */
	public void setFname(String fname) {
		this.fname = fname;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#getLname()
	 */
	public String getLname() {
		return lname;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#setLname(java.lang.String)
	 */
	public void setLname(String lname) {
		this.lname = lname;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#getGebdat()
	 */
	public String getGebdat() {
		return gebdat;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#setGebdat(java.lang.String)
	 */
	public void setGebdat(String gebdat) {
		this.gebdat = gebdat;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#getNationality()
	 */
	public String getNationality() {
		return nationality;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#setNationality(java.lang.String)
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#getRole()
	 */
	public String getRole() {
		return role;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#setRole(java.lang.String)
	 */
	public void setRole(String role) {
		this.role = role;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#getRace()
	 */
	public String getRace() {
		return race;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#setRace(java.lang.String)
	 */
	public void setRace(String race) {
		this.race = race;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#getTalent()
	 */
	public String getTalent() {
		return talent;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#setTalent(java.lang.String)
	 */
	public void setTalent(String talent) {
		this.talent = talent;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#getGender()
	 */
	public String getGender() {
		return gender;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#setGender(java.lang.String)
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#getAge()
	 */
	public int getAge() {
		return age;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#setAge(int)
	 */
	public void setAge(int age) {
		this.age = age;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#getHeight()
	 */
	public int getHeight() {
		return height;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#setHeight(int)
	 */
	public void setHeight(int height) {
		this.height = height;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#getWeight()
	 */
	public int getWeight() {
		return weight;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#setWeight(int)
	 */
	public void setWeight(int weight) {
		this.weight = weight;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#getBpoints()
	 */
	public int getBpoints() {
		return bpoints;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#setBpoints(int)
	 */
	public void setBpoints(int bpoints) {
		this.bpoints = bpoints;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#getApoints()
	 */
	public int getApoints() {
		return apoints;
	}
	/* (non-Javadoc)
	 * @see Logik.CharakterInterface#setApoints(int)
	 */
	public void setApoints(int apoints) {
		this.apoints = apoints;
	}
	
}
