package Logik;

public class User implements UserInterface{
	
	public User(String name, String passwort) {
		super();
		this.name = name;
		this.passwort = passwort;
	}
	
	String user_id;
	String name;
	String passwort;
	
	/* (non-Javadoc)
	 * @see Logik.UserInterface#getUser_id()
	 */
	public String getUser_id() {
		return user_id;
	}
	/* (non-Javadoc)
	 * @see Logik.UserInterface#setUser_id(java.lang.String)
	 */
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	/* (non-Javadoc)
	 * @see Logik.UserInterface#getName()
	 */
	public String getName() {
		return name;
	}
	/* (non-Javadoc)
	 * @see Logik.UserInterface#setName(java.lang.String)
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	public String getPasswort() {
		return passwort;
	}
	
	public void setPasswort(String passwort) {
		this.passwort = passwort;
	}
	
	public static boolean checkUser(User ui) {
		String[][] us = Anbindung.DBUser.getUser();
		
		for (int i = 0; i < us.length; i++) {
			if ((ui.getName().equals(us[i][1]))&&(ui.getPasswort().equals(us[i][2]))) {
				return true;
			}
		}
		
		return false;
		
	}
}
