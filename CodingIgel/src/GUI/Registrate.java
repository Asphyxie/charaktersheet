package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Anbindung.DBUser;
import Logik.User;
import Logik.UserInterface;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Registrate extends JFrame{

	private JPanel contentPane;
	private JTextField username;
	private JTextField password;
	private Benutzermenue be = new Benutzermenue();
	private DBUser user = new DBUser(); 
	private JButton button;
	/**
	 * @return the textField_1
	 */
	public JTextField getTextField_1() {
		return password;
	}

	/**
	 * @param textField_1 the textField_1 to set
	 */
	public void setTextField_1(JTextField textField_1) {
		this.password = textField_1;
	}
	/**
	 * @return the textField_1
	 */
	public JTextField getTextField() {
		return username;
	}
	/**
	 * @param textField the textField to set
	 */
	public void setTextField(JTextField textField) {
		this.username = textField;
	}
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Registrate frame = new Registrate();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Registrate() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 502, 267);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		username = new JTextField();
		username.setColumns(10);
		
		password = new JTextField();
		password.setColumns(10);
		
		JLabel lblBenutzernamenFestlegen = new JLabel("Benutzernamen festlegen:");
		
		JLabel lblPasswortFestlegen = new JLabel("Passwort festlegen:");
		
		JButton btnNewButton = new JButton("registrieren");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Anbindung.DBUser.addUser((String)username.getText(), (String)password.getText());
				System.out.println("Erfolgreich angemeldet!");
				
				Registrate.this.setName(username.getText());
				Registrate.this.setName(password.getText());
				be.setVisible(true);
				setVisible(false);
			}
		});
		
		button = new JButton("<----");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Login login = new Login();
				
				setVisible(false);
				login.setVisible(true);
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(51, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblPasswortFestlegen)
								.addComponent(lblBenutzernamenFestlegen)
								.addComponent(password, GroupLayout.PREFERRED_SIZE, 379, GroupLayout.PREFERRED_SIZE)
								.addComponent(username, GroupLayout.PREFERRED_SIZE, 379, GroupLayout.PREFERRED_SIZE))
							.addGap(46))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE)
							.addGap(182))))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(button)
					.addContainerGap(387, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(button)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblBenutzernamenFestlegen)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(username, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblPasswortFestlegen)
					.addGap(7)
					.addComponent(password, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
					.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
					.addGap(24))
		);
		contentPane.setLayout(gl_contentPane);
	}

}
