package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Logik.BPunkte;
import Logik.Charakter;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.JFormattedTextField;
import javax.swing.JSpinner;

public class Editor extends JFrame {

	private JPanel contentPane;
	private JTextField name;
	private JTextField vorname;
	private JTextField gebdat;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Editor frame = new Editor();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Editor() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 692, 842);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblCharakterEditor = new JLabel("Charakter Editor");
		lblCharakterEditor.setFont(new Font("Tahoma", Font.BOLD, 30));
		
		JLabel lblName = new JLabel("Name:");
		
		JLabel lblVorname = new JLabel("Vorname:");
		
		JLabel lblNation = new JLabel("Nation:");
		
		JLabel lblGeburtstag = new JLabel("Geburtsdatum:");
		
		JLabel lblSpezies = new JLabel("Spezies:");
		
		JLabel lblAlter = new JLabel("Alter:");
		
		JLabel lblGeschlecht = new JLabel("Geschlecht:");
		
		name = new JTextField();
		name.setColumns(10);
		
		vorname = new JTextField();
		vorname.setColumns(10);
		
		gebdat = new JTextField();
		gebdat.setColumns(10);
		
		JLabel lblAllgemein = new JLabel("Allgemeine Informationen:");
		lblAllgemein.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JScrollPane scrollPane = new JScrollPane();
		
		JButton btnSpeichern = new JButton("Speichern");
		
		
		JLabel lbllKlasse = new JLabel("Klasse:");
		
		JLabel lblTalente = new JLabel("Talente:");
		
		String[][] talente = Anbindung.DBTalent.getTalente();
		String[] talent_bezeichnung = new String[talente.length];
		for (int i = 0; i < talente.length; i++) {
			talent_bezeichnung[i] = talente[i][1];
		}
		JComboBox comboBox_talent = new JComboBox(talent_bezeichnung);
		comboBox_talent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		
		JLabel lblGroesse = new JLabel("Groesse:");
		
		JLabel lblGewicht = new JLabel("Gewicht:");
		
		JSeparator separator = new JSeparator();
		separator.setOrientation(SwingConstants.VERTICAL);
		
		JLabel lblBasispunkte = new JLabel("Basispunkte:");
		
		JLabel lblMu = new JLabel("MU");
		
		JLabel lblKl = new JLabel("KL");
		
		JLabel lblIn = new JLabel("IN");
		
		JSpinner MU = new JSpinner();
		
		JSpinner KL = new JSpinner();
		
		JSpinner IN = new JSpinner();
		
		JLabel lblCh = new JLabel("CH");
		
		JSpinner CH = new JSpinner();
		
		JLabel lblFf = new JLabel("FF");
		
		JSpinner FF = new JSpinner();
		
		JLabel lblGe = new JLabel("GE");
		
		JSpinner GE = new JSpinner();
		
		JLabel lblKo = new JLabel("KO");
		
		JSpinner KO = new JSpinner();
		
		JLabel lblKk = new JLabel("KK");
		
		JSpinner KK = new JSpinner();
		
		JLabel lblAbendteuerpunkte = new JLabel("Abenteuerpunkte:");
		
		JSpinner apunkte = new JSpinner();
		
		String[][] nationen = Anbindung.DBNationalitaet.getNationalities();
		String[] nation_bezeichnung = new String[nationen.length];
		for (int i = 0; i < nationen.length; i++) {
			nation_bezeichnung[i] = nationen[i][1];
		}
		 
		JComboBox comboBox_nation = new JComboBox(nation_bezeichnung);
		comboBox_nation.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent arg0) {
	    		
	    	}
	    });
		
		String[][] spezien = Anbindung.DBSpezies.getSpezies();
		String[] spezies_bezeichnung = new String[spezien.length];
		for (int i = 0; i < spezien.length; i++) {
			spezies_bezeichnung[i] = spezien[i][1];
		}
		JComboBox comboBox_spezies = new JComboBox(spezies_bezeichnung);
		comboBox_spezies.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		
		String[][] klassen = Anbindung.DBRolle.getRollen();
		String[] klasse_bezeichnung = new String[klassen.length];
		for (int i = 0; i < klassen.length; i++) {
			klasse_bezeichnung[i] = klassen[i][1];
		}
		JComboBox comboBox_klasse = new JComboBox(klasse_bezeichnung);
		comboBox_klasse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		
		JSpinner groesse = new JSpinner();
		
		JSpinner gewicht = new JSpinner();
		
		JSpinner alter = new JSpinner();
		
		String[] geschlecht_bezeichnung = {"Mann", "Frau"};
		JComboBox comboBox_geschlecht = new JComboBox(geschlecht_bezeichnung);
		comboBox_geschlecht.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		
		btnSpeichern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Charakter c = new Charakter(
						vorname.getText(), 
						name.getText(), 
						gebdat.getText(),
						(String)comboBox_nation.getSelectedItem(),
						(String)comboBox_klasse.getSelectedItem(),
						(String)comboBox_spezies.getSelectedItem(),
						(String)comboBox_talent.getSelectedItem(),
						(String)comboBox_geschlecht.getSelectedItem(),
						(int)alter.getValue(),
						(int)groesse.getValue(),
						(int)gewicht.getValue(),
						(int)apunkte.getValue(),
						(int)apunkte.getValue()						
						);
				BPunkte b = new BPunkte(
						(int)MU.getValue(),
						(int)KL.getValue(),
						(int)IN.getValue(),
						(int)CH.getValue(),
						(int)FF.getValue(),
						(int)GE.getValue(),
						(int)KO.getValue(),
						(int)KK.getValue()
						);
				Anbindung.DBCharakter.addChar(c);
			}
		});
		
		JButton btnDrucken = new JButton("Als Textdokument abspeichern");
		btnDrucken.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		
		JButton button = new JButton("<---");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Benutzermenue b = new Benutzermenue();
				
				setVisible(false);
				b.setVisible(true);
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(30)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
										.addComponent(btnDrucken, GroupLayout.PREFERRED_SIZE, 212, GroupLayout.PREFERRED_SIZE)
										.addGroup(gl_contentPane.createSequentialGroup()
											.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
												.addComponent(lblVorname, GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE)
												.addComponent(lblName)
												.addComponent(lblNation, GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE))
											.addPreferredGap(ComponentPlacement.RELATED)
											.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
												.addComponent(comboBox_nation, Alignment.TRAILING, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addComponent(name, Alignment.TRAILING, 0, 0, Short.MAX_VALUE)
												.addComponent(vorname, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 176, Short.MAX_VALUE)))
										.addGroup(gl_contentPane.createSequentialGroup()
											.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
												.addComponent(lblGeburtstag, GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE)
												.addComponent(lblSpezies, GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE))
											.addPreferredGap(ComponentPlacement.RELATED)
											.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
												.addComponent(comboBox_spezies, Alignment.TRAILING, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addComponent(gebdat, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 176, Short.MAX_VALUE)))
										.addGroup(gl_contentPane.createSequentialGroup()
											.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
												.addGroup(gl_contentPane.createSequentialGroup()
													.addComponent(lblAlter, GroupLayout.DEFAULT_SIZE, 375, Short.MAX_VALUE)
													.addPreferredGap(ComponentPlacement.UNRELATED))
												.addGroup(gl_contentPane.createSequentialGroup()
													.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
														.addComponent(lblGeschlecht, GroupLayout.DEFAULT_SIZE, 366, Short.MAX_VALUE)
														.addComponent(lbllKlasse, Alignment.LEADING))
													.addGap(19))
												.addGroup(gl_contentPane.createSequentialGroup()
													.addComponent(lblTalente)
													.addPreferredGap(ComponentPlacement.RELATED, 309, Short.MAX_VALUE)
													.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
													.addGap(34)))
											.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
												.addComponent(comboBox_talent, Alignment.TRAILING, 0, 176, Short.MAX_VALUE)
												.addComponent(comboBox_geschlecht, Alignment.TRAILING, 0, 176, Short.MAX_VALUE)
												.addComponent(comboBox_klasse, Alignment.TRAILING, 0, 176, Short.MAX_VALUE)
												.addComponent(alter, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 176, Short.MAX_VALUE)
												.addComponent(groesse, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(gewicht, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
									.addGap(78))
								.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
									.addComponent(lblGewicht)
									.addComponent(lblGroesse))))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(205)
							.addComponent(lblCharakterEditor))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(42)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(separator, GroupLayout.PREFERRED_SIZE, 329, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(lblBasispunkte)
									.addGap(18)
									.addComponent(lblMu))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(lblAbendteuerpunkte)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(apunkte, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGap(69)
									.addComponent(btnSpeichern, GroupLayout.PREFERRED_SIZE, 198, GroupLayout.PREFERRED_SIZE)))
							.addGap(298))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(69)
							.addComponent(lblAllgemein, GroupLayout.PREFERRED_SIZE, 247, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(118)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
								.addComponent(MU, GroupLayout.DEFAULT_SIZE, 41, Short.MAX_VALUE)
								.addComponent(lblGe)
								.addComponent(GE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(18)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
									.addComponent(KL, GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
									.addGroup(gl_contentPane.createSequentialGroup()
										.addComponent(lblKl)
										.addPreferredGap(ComponentPlacement.RELATED))
									.addComponent(KO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addComponent(lblKo))
							.addGap(18)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
										.addComponent(IN, GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
										.addComponent(lblIn)
										.addComponent(KK, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
									.addGap(18)
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
										.addComponent(CH, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
										.addComponent(lblCh))
									.addGap(18)
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
										.addComponent(lblFf)
										.addComponent(FF, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)))
								.addComponent(lblKk)))
						.addComponent(button))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblCharakterEditor))
						.addComponent(button))
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_contentPane.createSequentialGroup()
								.addGap(41)
								.addComponent(name, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addComponent(lblName, Alignment.TRAILING))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(lblAllgemein, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblVorname)
						.addComponent(vorname, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNation)
						.addComponent(comboBox_nation, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblGeburtstag)
						.addComponent(gebdat, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblSpezies)
						.addComponent(comboBox_spezies, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAlter)
						.addComponent(alter, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblGeschlecht)
						.addComponent(comboBox_geschlecht, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lbllKlasse)
						.addComponent(comboBox_klasse, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(6)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(13)
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(comboBox_talent, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblTalente))))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblGroesse)
						.addComponent(groesse, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblGewicht)
						.addComponent(gewicht, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(35)
					.addComponent(separator, GroupLayout.PREFERRED_SIZE, 9, GroupLayout.PREFERRED_SIZE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(lblBasispunkte))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(18)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblCh)
								.addComponent(lblFf, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblKl)
								.addComponent(lblIn)
								.addComponent(lblMu))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(MU, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(KL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(IN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(CH, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(FF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(18)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblKk)
								.addComponent(lblKo)
								.addComponent(lblGe))
							.addGap(4)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(GE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(KO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(KK, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
					.addGap(29)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(apunkte, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblAbendteuerpunkte))
					.addPreferredGap(ComponentPlacement.RELATED, 157, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(btnSpeichern, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnDrucken, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
	}
}
