package GUI;

import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JLabel;
import java.awt.Font;

public class MainMenuPanel extends JPanel {

	/**
	 * Create the panel.
	 */
	public MainMenuPanel() {
		setBackground(Color.BLACK);
		
		JButton btnNeuenCharakterErstellen = new JButton("Neuen Charakter erstellen");
		
		JButton btnAbmelden = new JButton("Abmelden");
		
		JLabel lblPenAndPaper = new JLabel("Pen and Paper");
		lblPenAndPaper.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblPenAndPaper.setForeground(Color.WHITE);
		
		JLabel lblCharaktereditor = new JLabel("Charaktereditor");
		lblCharaktereditor.setForeground(Color.WHITE);
		lblCharaktereditor.setFont(new Font("Tahoma", Font.PLAIN, 20));
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(322)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblCharaktereditor, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblPenAndPaper))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
					.addGap(78)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(btnAbmelden, GroupLayout.PREFERRED_SIZE, 222, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnNeuenCharakterErstellen, GroupLayout.PREFERRED_SIZE, 222, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(239, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
					.addGap(34)
					.addComponent(lblPenAndPaper)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblCharaktereditor, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
					.addGap(81)
					.addComponent(btnNeuenCharakterErstellen, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
					.addGap(40)
					.addComponent(btnAbmelden, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(143, Short.MAX_VALUE))
		);
		setLayout(groupLayout);

	}
}
