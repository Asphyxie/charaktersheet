package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.JLabel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class Benutzermenue extends JFrame {

	private JPanel contentPane;
	private Editor ed = new Editor();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Benutzermenue frame = new Benutzermenue();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Benutzermenue() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 496, 622);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(5, 1, 0, 0));
		ImageIcon icon = new ImageIcon(Benutzermenue.class.getResource("/"));		//Auch hier unser Logo
		icon.setImage(icon.getImage().getScaledInstance(490, 110, Image.SCALE_DEFAULT));
		
		JLabel lblBenutzermen = new JLabel("Benutzermenue");
		lblBenutzermen.setFont(new Font("Tahoma", Font.PLAIN, 30));
		contentPane.add(lblBenutzermen);
		
		JButton btnAnzeigen = new JButton("Charaktere anzeigen");
		btnAnzeigen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ErstellteCharaktere anzeigen1 = new ErstellteCharaktere();				//Klasse Liste erstellen die alle Charaktere eines Users ausgibt
				anzeigen1.setVisible(true);
			}
		});
		contentPane.add(btnAnzeigen);
		
		JButton btnErstellen = new JButton("neuen Charakter erstellen");
		btnErstellen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mainMenu erstellen1 = new mainMenu();
				erstellen1.setVisible(false);
				ed.setVisible(true);
				setVisible(false);	
			}
		});
		contentPane.add(btnErstellen);
		
		// JButton btnAusdrucken = new JButton("Charaktere ausdrucken");		//Klasse Ausdrucken existiert noch nocht
		// btnAusdrucken.addActionListener(new ActionListener() {
			// public void actionPerformed(ActionEvent arg0) {
			//	Ausdrucken ausdrucken1 = new Ausdrucken();
			//	ausdrucken1.setVisible(true);
			//	setVisible(false);
			
		// contentPane.add(btnAusdrucken);
		
		JButton btnAusloggen = new JButton("Ausloggen");
		btnAusloggen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Ausloggen_Clicked();
			}
		});
		contentPane.add(btnAusloggen);
	}

	public void Ausloggen_Clicked() {		//Dass sich das Programm dann schlie�t
		Login login = new Login();
		
		setVisible(false);
		login.setVisible(true); 
	}
	
}
