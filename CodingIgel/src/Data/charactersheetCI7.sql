-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 27. Nov 2018 um 19:38
-- Server-Version: 10.1.31-MariaDB
-- PHP-Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `charactersheet`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `t_apunkte`
--

CREATE TABLE `t_apunkte` (
  `P_apunkte_id` int(11) NOT NULL,
  `anzahl` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten f�r Tabelle `t_apunkte`
--

INSERT INTO `t_apunkte` (`P_apunkte_id`, `anzahl`) VALUES
(80001, 900),
(80002, 900),
(80003, 1400),
(80004, 1100),
(80005, 1200),
(80006, 1200),
(80007, 1200),
(80008, 1200);

-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `t_bpunkte`
--

CREATE TABLE `t_bpunkte` (
  `P_bpunkte_id` int(11) NOT NULL,
  `mut` int(15) DEFAULT NULL,
  `klugheit` int(15) DEFAULT NULL,
  `intuition` int(15) DEFAULT NULL,
  `charisma` int(15) DEFAULT NULL,
  `fingerfertigkeit` int(15) DEFAULT NULL,
  `geschicklichkeit` int(15) DEFAULT NULL,
  `kondition` int(15) DEFAULT NULL,
  `koerperkraft` int(15) DEFAULT NULL,
  `grenze` int(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten f�r Tabelle `t_bpunkte`
--

INSERT INTO `t_bpunkte` (`P_bpunkte_id`, `mut`, `klugheit`, `intuition`, `charisma`, `fingerfertigkeit`, `geschicklichkeit`, `kondition`, `koerperkraft`, `grenze`) VALUES
(15, 13, 14, 12, 14, 13, 12, 10, 12, 100),
(16, 12, 10, 12, 13, 14, 12, 14, 13, 100),
(17, 14, 14, 12, 12, 13, 12, 13, 10, 100),
(18, 12, 14, 13, 12, 13, 14, 12, 10, 100),
(19, 10, 12, 12, 12, 13, 13, 14, 14, 100),
(20, 14, 14, 13, 13, 12, 12, 12, 10, 100),
(21, 11, 13, 14, 14, 13, 12, 11, 12, 100),
(22, 12, 11, 12, 13, 14, 14, 13, 12, 100);

-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `t_charaktere`
--

CREATE TABLE `t_charaktere` (
  `P_charakter_id` int(11) NOT NULL,
  `fname` varchar(50) DEFAULT NULL,
  `lname` varchar(50) DEFAULT NULL,
  `gebdat` varchar(25) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `nationalitaet` varchar(50) DEFAULT NULL,
  `rolle` varchar(50) DEFAULT NULL,
  `spezies` varchar(50) DEFAULT NULL,
  `talent` varchar(50) DEFAULT NULL,
  `geschlecht` varchar(1) DEFAULT NULL,
  `groesse` int(11) DEFAULT NULL,
  `gewicht` int(11) DEFAULT NULL,
  `bpunkte` int(11) DEFAULT NULL,
  `apunkte` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten f�r Tabelle `t_charaktere`
--

INSERT INTO `t_charaktere` (`P_charakter_id`, `fname`, `lname`, `gebdat`, `age`, `nationalitaet`, `rolle`, `spezies`, `talent`, `geschlecht`, `groesse`, `gewicht`, `bpunkte`, `apunkte`) VALUES
(22, 'Kusmara', 'Jezcardo', '03.01.92', 26, NULL, NULL, NULL, NULL, 'w', 78, 46, 100, 900),
(23, 'Alari', 'Regenbogen', '24.09.91', 27, NULL, NULL, NULL, NULL, 'w', 79, 46, 100, 900),
(24, 'Torlif', 'Bjarnison', '15.02.92', 26, NULL, NULL, NULL, NULL, 'm', 105, 110, 100, 1400),
(25, 'Asmodena', 'Scaevola', '02.07.93', 25, NULL, NULL, NULL, NULL, 'w', 81, 56, 100, 1100),
(26, 'Prigoryn', 'Jarsyn', '09.05.93', 25, NULL, NULL, NULL, NULL, 'm', 90, 80, 100, 1200),
(27, 'Gierlap', 'Reichenau', '10.12.97', 21, NULL, NULL, NULL, NULL, 'm', 84, 59, 100, 1200),
(28, 'Staekel', 'Unflatswurzl', '04.02.97', 21, NULL, NULL, NULL, NULL, 'm', 102, 109, 100, 1200),
(29, 'Blähdarm', 'Giergalgen', '22.11.99', 18, NULL, NULL, NULL, NULL, 'm', 94, 82, 100, 1200);

-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `t_charaktere_apunkte`
--

CREATE TABLE `t_charaktere_apunkte` (
  `F_charakter_id` int(11) DEFAULT NULL,
  `F_apunkte_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `t_charaktere_bpunkte`
--

CREATE TABLE `t_charaktere_bpunkte` (
  `F_charakter_id` int(11) DEFAULT NULL,
  `F_bpunkte_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `t_charaktere_nationalitaeten`
--

CREATE TABLE `t_charaktere_nationalitaeten` (
  `F_charakter_id` int(11) DEFAULT NULL,
  `F_national_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `t_charaktere_rollen`
--

CREATE TABLE `t_charaktere_rollen` (
  `F_charakter_id` int(11) DEFAULT NULL,
  `F_rollen_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `t_charaktere_spezies`
--

CREATE TABLE `t_charaktere_spezies` (
  `F_charakter_id` int(11) DEFAULT NULL,
  `F_spezies_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `t_charaktere_talente`
--

CREATE TABLE `t_charaktere_talente` (
  `F_charakter_id` int(11) DEFAULT NULL,
  `F_talent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `t_charaktere_user`
--

CREATE TABLE `t_charaktere_user` (
  `F_charakter_id` int(11) DEFAULT NULL,
  `F_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `t_nationalitaeten`
--

CREATE TABLE `t_nationalitaeten` (
  `p_national_id` int(11) NOT NULL,
  `bezeichnung` varchar(50) DEFAULT NULL,
  `sprache` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten f�r Tabelle `t_nationalitaeten`
--

INSERT INTO `t_nationalitaeten` (`p_national_id`, `bezeichnung`, `sprache`) VALUES
(30001, 'Garethi', 'Bornisch'),
(30002, 'Garethi', 'Aureliani'),
(30003, 'Tulamidya', 'Balashidisch'),
(30004, 'Thorwalsch', 'Gjalskisch'),
(30005, 'Elfisch', 'Isdira'),
(30006, 'Rissoal', 'Mahrisch'),
(30007, 'Orkisch', 'Oloarkh'),
(30008, 'Waldmenschen', 'Petaya');

-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `t_rollen`
--

CREATE TABLE `t_rollen` (
  `p_rollen_id` int(11) NOT NULL,
  `bezeichnung` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten f�r Tabelle `t_rollen`
--

INSERT INTO `t_rollen` (`p_rollen_id`, `bezeichnung`) VALUES
(50001, 'Gaukler/in'),
(50002, 'H�ndler/in'),
(50003, 'J�ger/in'),
(50004, 'Medicus'),
(50005, 'Dieb/in'),
(50006, 'Waffenschmied/in'),
(50007, 'Krieger/in'),
(50008, 'Gardist/in'),
(50009, 'Magier/in'),
(50010, 'Berserker/in');

-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `t_spezies`
--

CREATE TABLE `t_spezies` (
  `p_spezies_id` int(11) NOT NULL,
  `bezeichnung` varchar(50) DEFAULT NULL,
  `einschraenkungen` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten f�r Tabelle `t_spezies`
--

INSERT INTO `t_spezies` (`p_spezies_id`, `bezeichnung`, `einschraenkungen`) VALUES
(40001, 'Mensch', 'Keine'),
(40002, 'Elf', 'Berserker/in'),
(40003, 'Zwerg', 'Magier/in'),
(40004, 'Halbelf', 'Berserker/in'),
(40005, 'Mensch', 'Keine'),
(40006, 'Mensch', 'Keine'),
(40007, 'Mensch', 'Keine'),
(40008, 'Mensch', 'Keine'),
(40009, 'Ork', 'Gaukler/in'),
(40010, 'Goblin', 'Gardist/in'),
(40011, 'Zwerge', 'Keine'),
(40012, 'Achaz', 'Keine'),
(40013, 'Halbork', 'Gaukler/in');

-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `t_talente`
--

CREATE TABLE `t_talente` (
  `p_talent_id` int(11) NOT NULL,
  `bezeichnung` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten f�r Tabelle `t_talente`
--

INSERT INTO `t_talente` (`p_talent_id`, `bezeichnung`) VALUES
(60001, 'F�hrtensuchen'),
(60002, 'Fallenstellen'),
(60003, 'Fischen'),
(60004, 'Reiten'),
(60005, 'Bet�ren'),
(60006, 'Kochen'),
(60007, 'Einsch�chtern'),
(60008, 'Gassenwissen'),
(60009, 'Menschenkenntnis'),
(60010, '�berreden'),
(60011, 'Verkleidern'),
(60012, 'Alchimie'),
(60013, 'Fliegen'),
(60014, 'Klettern'),
(60015, 'Schwimmen'),
(60016, 'Taschendiebstahl');

-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `t_user`
--

CREATE TABLE `t_user` (
  `P_user_id` int(11) NOT NULL,
  `name` varchar(16) DEFAULT NULL,
  `passwort` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten f�r Tabelle `t_user`
--

INSERT INTO `t_user` (`P_user_id`, `name`, `passwort`) VALUES
(1, 'Tim', '7c4a8d09ca3762af'),
(2, 'Katrin', 'dd5fef9c1c1da139'),
(3, 'Steffen', '0ba759782039b1a4'),
(4, 'Miriam', 'd7e4e9abedd0949b'),
(5, 'Stefan', '82a19184099e2aac'),
(6, 'Jan', 'e4240f86276c7170'),
(7, 'Leo', 'f18f057ea44a945a');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes f�r die Tabelle `t_apunkte`
--
ALTER TABLE `t_apunkte`
  ADD PRIMARY KEY (`P_apunkte_id`);

--
-- Indizes f�r die Tabelle `t_bpunkte`
--
ALTER TABLE `t_bpunkte`
  ADD PRIMARY KEY (`P_bpunkte_id`);

--
-- Indizes f�r die Tabelle `t_charaktere`
--
ALTER TABLE `t_charaktere`
  ADD PRIMARY KEY (`P_charakter_id`);

--
-- Indizes f�r die Tabelle `t_charaktere_apunkte`
--
ALTER TABLE `t_charaktere_apunkte`
  ADD KEY `PUNKTE_fk_charakter_id` (`F_charakter_id`),
  ADD KEY `PUNKTE_fk_apunkte_id` (`F_apunkte_id`);

--
-- Indizes f�r die Tabelle `t_charaktere_bpunkte`
--
ALTER TABLE `t_charaktere_bpunkte`
  ADD KEY `PUNKT_fk_charakter_id` (`F_charakter_id`),
  ADD KEY `PUNKT_fk_bpunkte_id` (`F_bpunkte_id`);

--
-- Indizes f�r die Tabelle `t_charaktere_nationalitaeten`
--
ALTER TABLE `t_charaktere_nationalitaeten`
  ADD KEY `fk_charakter_id` (`F_charakter_id`),
  ADD KEY `fk_national_id` (`F_national_id`);

--
-- Indizes f�r die Tabelle `t_charaktere_rollen`
--
ALTER TABLE `t_charaktere_rollen`
  ADD KEY `ROLL_fk_charakter_id` (`F_charakter_id`),
  ADD KEY `ROLL_fk_rollen_id` (`F_rollen_id`);

--
-- Indizes f�r die Tabelle `t_charaktere_talente`
--
ALTER TABLE `t_charaktere_talente`
  ADD KEY `INDEX_fk_charakter_id` (`F_charakter_id`),
  ADD KEY `INDEX_fk_talent_id` (`F_talent_id`);

--
-- Indizes f�r die Tabelle `t_charaktere_user`
--
ALTER TABLE `t_charaktere_user`
  ADD KEY `USER_fk_charakter_id` (`F_charakter_id`),
  ADD KEY `USER_fk_user_id` (`F_user_id`);

--
-- Indizes f�r die Tabelle `t_nationalitaeten`
--
ALTER TABLE `t_nationalitaeten`
  ADD PRIMARY KEY (`p_national_id`);

--
-- Indizes f�r die Tabelle `t_rollen`
--
ALTER TABLE `t_rollen`
  ADD PRIMARY KEY (`p_rollen_id`);

--
-- Indizes f�r die Tabelle `t_spezies`
--
ALTER TABLE `t_spezies`
  ADD PRIMARY KEY (`p_spezies_id`);

--
-- Indizes f�r die Tabelle `t_talente`
--
ALTER TABLE `t_talente`
  ADD PRIMARY KEY (`p_talent_id`);

--
-- Indizes f�r die Tabelle `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`P_user_id`);

--
-- AUTO_INCREMENT f�r exportierte Tabellen
--

--
-- AUTO_INCREMENT f�r Tabelle `t_bpunkte`
--
ALTER TABLE `t_bpunkte`
  MODIFY `P_bpunkte_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT f�r Tabelle `t_charaktere`
--
ALTER TABLE `t_charaktere`
  MODIFY `P_charakter_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT f�r Tabelle `t_user`
--
ALTER TABLE `t_user`
  MODIFY `P_user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `t_charaktere_apunkte`
--
ALTER TABLE `t_charaktere_apunkte`
  ADD CONSTRAINT `PUNKTE_fk_apunkte_id` FOREIGN KEY (`F_apunkte_id`) REFERENCES `t_apunkte` (`P_apunkte_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `PUNKTE_fk_charakter_id` FOREIGN KEY (`F_charakter_id`) REFERENCES `t_charaktere` (`P_charakter_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints der Tabelle `t_charaktere_bpunkte`
--
ALTER TABLE `t_charaktere_bpunkte`
  ADD CONSTRAINT `PUNKT_fk_bpunkte_id` FOREIGN KEY (`F_bpunkte_id`) REFERENCES `t_bpunkte` (`P_bpunkte_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `PUNKT_fk_charakter_id` FOREIGN KEY (`F_charakter_id`) REFERENCES `t_charaktere` (`P_charakter_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints der Tabelle `t_charaktere_nationalitaeten`
--
ALTER TABLE `t_charaktere_nationalitaeten`
  ADD CONSTRAINT `fk_charakter_id` FOREIGN KEY (`F_charakter_id`) REFERENCES `t_charaktere` (`P_charakter_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_national_id` FOREIGN KEY (`F_national_id`) REFERENCES `t_nationalitaeten` (`p_national_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints der Tabelle `t_charaktere_rollen`
--
ALTER TABLE `t_charaktere_rollen`
  ADD CONSTRAINT `ROLL_fk_charakter_id` FOREIGN KEY (`F_charakter_id`) REFERENCES `t_charaktere` (`P_charakter_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `ROLL_fk_rollen_id` FOREIGN KEY (`F_rollen_id`) REFERENCES `t_rollen` (`p_rollen_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints der Tabelle `t_charaktere_talente`
--
ALTER TABLE `t_charaktere_talente`
  ADD CONSTRAINT `CONST_fk_charakter_id` FOREIGN KEY (`F_charakter_id`) REFERENCES `t_charaktere` (`P_charakter_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `CONST_fk_talent_id` FOREIGN KEY (`F_talent_id`) REFERENCES `t_talente` (`p_talent_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints der Tabelle `t_charaktere_user`
--
ALTER TABLE `t_charaktere_user`
  ADD CONSTRAINT `USER_fk_charakter_id` FOREIGN KEY (`F_charakter_id`) REFERENCES `t_charaktere` (`P_charakter_id`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
