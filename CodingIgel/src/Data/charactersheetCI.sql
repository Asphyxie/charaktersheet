-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 18. Nov 2018 um 17:59
-- Server-Version: 10.1.31-MariaDB
-- PHP-Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `charactersheet`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `t_charaktere`
--

CREATE TABLE `t_charaktere` (
  `p_charakter_id` varchar(10) NOT NULL,
  `fname` varchar(50) DEFAULT NULL,
  `lname` varchar(50) DEFAULT NULL,
  `gebdat` varchar(25) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `nationalitaet` varchar(50) DEFAULT NULL,
  `rolle` varchar(50) DEFAULT NULL,
  `spezies` varchar(50) DEFAULT NULL,
  `talent` varchar(50) DEFAULT NULL,
  `geschlecht` varchar(1) DEFAULT NULL,
  `groesse` int(11) DEFAULT NULL,
  `gewicht` int(11) DEFAULT NULL,
  `bpunkte` int(11) DEFAULT NULL,
  `apunkte` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `t_charaktere`
--

INSERT INTO `t_charaktere` (`p_charakter_id`, `fname`, `lname`, `gebdat`, `age`, `nationalitaet`, `rolle`, `spezies`, `talent`, `geschlecht`, `groesse`, `gewicht`, `bpunkte`, `apunkte`) VALUES
('20001', 'Kusmara', 'Jezcardo', '03.01.92', 26, NULL, NULL, NULL, NULL, 'w', 78, 46, 100, 900),
('20002', 'Alari', 'Regenbogen', '24.09.91', 27, NULL, NULL, NULL, NULL, 'w', 79, 46, 100, 900),
('20003', 'Torlif', 'Bjarnison', '15.02.92', 26, NULL, NULL, NULL, NULL, 'm', 105, 110, 100, 1400),
('20004', 'Asmodena', 'Scaevola', '02.07.93', 25, NULL, NULL, NULL, NULL, 'w', 81, 56, 100, 1100),
('20005', 'Prigoryn', 'Jarsyn', '09.05.93', 25, NULL, NULL, NULL, NULL, 'm', 90, 80, 100, 1200),
('20006', 'Gierlap', 'Reichenau', '10.12.97', 21, NULL, NULL, NULL, NULL, 'm', 84, 59, 100, 1200),
('20007', 'Staekel', 'Unflatswurzl', '04.02.97', 21, NULL, NULL, NULL, NULL, 'm', 102, 109, 100, 1200),
('20008', 'Blähdarm', 'Giergalgen', '22.11.99', 18, NULL, NULL, NULL, NULL, 'm', 94, 82, 100, 1200);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `t_charaktere_nationalitaeten`
--

CREATE TABLE `t_charaktere_nationalitaeten` (
  `F_charakter_id` varchar(10) DEFAULT NULL,
  `F_national_id` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `t_charaktere_rollen`
--

CREATE TABLE `t_charaktere_rollen` (
  `F_charakter_id` varchar(10) DEFAULT NULL,
  `F_rollen_id` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `t_charaktere_spezies`
--

CREATE TABLE `t_charaktere_spezies` (
  `F_charakter_id` varchar(10) DEFAULT NULL,
  `F_spezies_id` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `t_charaktere_talente`
--

CREATE TABLE `t_charaktere_talente` (
  `F_charakter_id` varchar(10) DEFAULT NULL,
  `F_talent_id` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `t_charaktere_user`
--

CREATE TABLE `t_charaktere_user` (
  `F_charakter_id` varchar(10) DEFAULT NULL,
  `F_user_id` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `t_nationalitaeten`
--

CREATE TABLE `t_nationalitaeten` (
  `p_national_id` varchar(10) NOT NULL,
  `bezeichnung` varchar(50) DEFAULT NULL,
  `sprache` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `t_nationalitaeten`
--

INSERT INTO `t_nationalitaeten` (`p_national_id`, `bezeichnung`, `sprache`) VALUES
('30001', 'Garethi', 'Bornisch'),
('30002', 'Garethi', 'Aureliani'),
('30003', 'Tulamidya', 'Balashidisch'),
('30004', 'Thorwalsch', 'Gjalskisch'),
('30005', 'Elfisch', 'Isdira'),
('30006', 'Rissoal', 'Mahrisch'),
('30007', 'Orkisch', 'Oloarkh'),
('30008', 'Waldmenschen', 'Petaya');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `t_rollen`
--

CREATE TABLE `t_rollen` (
  `p_rollen_id` varchar(10) NOT NULL,
  `bezeichnung` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `t_rollen`
--

INSERT INTO `t_rollen` (`p_rollen_id`, `bezeichnung`) VALUES
('50001', 'Gaukler/in'),
('50002', 'Händler/in'),
('50003', 'Jäger/in'),
('50004', 'Medicus'),
('50005', 'Dieb/in'),
('50006', 'Waffenschmied/in'),
('50007', 'Krieger/in'),
('50008', 'Gardist/in'),
('50009', 'Magier/in');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `t_spezies`
--

CREATE TABLE `t_spezies` (
  `p_spezies_id` varchar(10) NOT NULL,
  `bezeichnung` varchar(50) DEFAULT NULL,
  `einschraenkungen` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `t_spezies`
--

INSERT INTO `t_spezies` (`p_spezies_id`, `bezeichnung`, `einschraenkungen`) VALUES
('40001', 'Mensch', 'Keine'),
('40002', 'Elf', 'Berserker'),
('40003', 'Zwerg', 'Magier'),
('40004', 'Halbelf', 'Berserker'),
('40005', 'Mensch', 'Keine'),
('40006', 'Mensch', 'Keine'),
('40007', 'Mensch', 'Keine'),
('40008', 'Mensch', 'Keine');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `t_talente`
--

CREATE TABLE `t_talente` (
  `p_talent_id` varchar(10) NOT NULL,
  `bezeichnung` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `t_talente`
--

INSERT INTO `t_talente` (`p_talent_id`, `bezeichnung`) VALUES
('60001', 'Fährtensuchen'),
('60002', 'Fallenstellen'),
('60003', 'Fischen'),
('60004', 'Reiten'),
('60005', 'Betören'),
('60006', 'Kochen');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `t_user`
--

CREATE TABLE `t_user` (
  `P_user_id` int(11) NOT NULL,
  `name` varchar(10) DEFAULT NULL,
  `passwort` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `t_user`
--

INSERT INTO `t_user` (`P_user_id`, `name`, `passwort`) VALUES
(1, 'Tim', '123456'),
(2, 'Katrin', '654321'),
(3, 'Steffen', '081500'),
(4, 'Miriam', '424242'),
(5, 'Stefan', '562346'),
(6, 'Jan', 'D3lf1n'),
(7, 'Leo', '456123');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `t_charaktere`
--
ALTER TABLE `t_charaktere`
  ADD PRIMARY KEY (`p_charakter_id`);

--
-- Indizes für die Tabelle `t_charaktere_nationalitaeten`
--
ALTER TABLE `t_charaktere_nationalitaeten`
  ADD KEY `fk_charakter_id` (`F_charakter_id`),
  ADD KEY `fk_national_id` (`F_national_id`);

--
-- Indizes für die Tabelle `t_charaktere_rollen`
--
ALTER TABLE `t_charaktere_rollen`
  ADD KEY `ROLL_fk_charakter_id` (`F_charakter_id`),
  ADD KEY `ROLL_fk_rollen_id` (`F_rollen_id`);

--
-- Indizes für die Tabelle `t_charaktere_talente`
--
ALTER TABLE `t_charaktere_talente`
  ADD KEY `INDEX_fk_charakter_id` (`F_charakter_id`),
  ADD KEY `INDEX_fk_talent_id` (`F_talent_id`);

--
-- Indizes für die Tabelle `t_charaktere_user`
--
ALTER TABLE `t_charaktere_user`
  ADD KEY `USER_fk_charakter_id` (`F_charakter_id`),
  ADD KEY `USER_fk_user_id` (`F_user_id`);

--
-- Indizes für die Tabelle `t_nationalitaeten`
--
ALTER TABLE `t_nationalitaeten`
  ADD PRIMARY KEY (`p_national_id`);

--
-- Indizes für die Tabelle `t_rollen`
--
ALTER TABLE `t_rollen`
  ADD PRIMARY KEY (`p_rollen_id`);

--
-- Indizes für die Tabelle `t_spezies`
--
ALTER TABLE `t_spezies`
  ADD PRIMARY KEY (`p_spezies_id`);

--
-- Indizes für die Tabelle `t_talente`
--
ALTER TABLE `t_talente`
  ADD PRIMARY KEY (`p_talent_id`);

--
-- Indizes für die Tabelle `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`P_user_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `t_user`
--
ALTER TABLE `t_user`
  MODIFY `P_user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `t_charaktere_nationalitaeten`
--
ALTER TABLE `t_charaktere_nationalitaeten`
  ADD CONSTRAINT `fk_charakter_id` FOREIGN KEY (`F_charakter_id`) REFERENCES `t_charaktere` (`p_charakter_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_national_id` FOREIGN KEY (`F_national_id`) REFERENCES `t_nationalitaeten` (`p_national_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints der Tabelle `t_charaktere_rollen`
--
ALTER TABLE `t_charaktere_rollen`
  ADD CONSTRAINT `ROLL_fk_charakter_id` FOREIGN KEY (`F_charakter_id`) REFERENCES `t_charaktere` (`p_charakter_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `ROLL_fk_rollen_id` FOREIGN KEY (`F_rollen_id`) REFERENCES `t_rollen` (`p_rollen_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints der Tabelle `t_charaktere_talente`
--
ALTER TABLE `t_charaktere_talente`
  ADD CONSTRAINT `CONST_fk_charakter_id` FOREIGN KEY (`F_charakter_id`) REFERENCES `t_charaktere` (`p_charakter_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `CONST_fk_talent_id` FOREIGN KEY (`F_talent_id`) REFERENCES `t_talente` (`p_talent_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints der Tabelle `t_charaktere_user`
--
ALTER TABLE `t_charaktere_user`
  ADD CONSTRAINT `USER_fk_charakter_id` FOREIGN KEY (`F_charakter_id`) REFERENCES `t_charaktere` (`p_charakter_id`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
