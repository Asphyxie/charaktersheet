package Data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import Logik.Charakter;
import Logik.CharakterInterface;

public class DBCodingIgel {
	public String getName() {

		String url = "jdbc:mysql://localhost/codingigel?"; //oder "localhost/charctersheet?"
		String user = "root";
		String passwort = "";
		// JDBC- Treiber registrieren
		String driver = "com.mysql.jdbc.Driver";
		String maxName = "";
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, passwort);
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM T_User;");

			// Ergebnis abfragen
			while (rs.next()) {
				// maximaler Wert aus der DB holen
				maxName = rs.getString(2);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return maxName;
}